// Turn off lint-warnings to not spam. I don't care about code-style in this file.
#![allow(unused)]
#![allow(bad_style)]

use bevy::prelude::*;
use bevy_rapier3d::prelude::*;

use crate::components::{player_ship::PlayerShip, projectile::Projectile};



pub struct DebugAndDoodlesPlugin;

impl Plugin for DebugAndDoodlesPlugin {
    fn build(&self, app: &mut App) {
        app
            // .add_system(DEBUG_display_ship_mass)
            // .add_system(DEBUG_count_projectiles)
            // .add_system(move_world_origin_to_player_pos)
            ;
    }
}

/// System, debug
fn DEBUG_display_ship_mass(
    commands: Commands,
    query: Query<(&Name, &RapierRigidBodyHandle), With<PlayerShip>>,
    rapier: Res<RapierContext>
) {
    if let Ok((name, handle)) = query.get_single() {
        let mass = rapier.bodies.get(handle.0).unwrap().mass();
        info!("{}: {}", name, mass);
    }
}

#[derive(Default)]
struct DEBUG_MaxCount(i32);

/// System, debug
fn DEBUG_count_projectiles(
    commands: Commands,
    query: Query<(Entity), With<Projectile>>,
    mut max_count: Local<DEBUG_MaxCount>
) {
    let count = query.iter().count() as i32;
    max_count.0 = std::cmp::max(count, max_count.0);
    info!("max: {}, curr: {}", max_count.0, count);
    // > max: 25000
}

/// System, doodle
/// 
/// # doodle result:
/// It seems that engine quite well handles global yanking of Transforms.
/// I think that i can go ahead and make game with division into chunks and move origin_point whenever player changes chunk.
/// Possible pitfall: be wary when doing soo cause world far from player still will have imprecissions that would have to be handled.
fn move_world_origin_to_player_pos(
    player: Query<(&GlobalTransform), With<PlayerShip>>,
    mut root_level_transforms: Query<(&mut Transform), Without<Parent>>,
) {
    let move_by = -player.get_single().unwrap().translation();
    root_level_transforms
        .par_iter_mut()
        .for_each_mut(|mut transform| transform.translation += move_by)
}