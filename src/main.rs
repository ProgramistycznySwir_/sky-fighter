
use bevy::{prelude::*, diagnostic::{FrameTimeDiagnosticsPlugin}, core_pipeline::{tonemapping::Tonemapping, bloom::BloomCompositeMode}};
use bevy_editor_pls::prelude::*;
use bevy_asset_loader::prelude::*;
use bevy_rapier3d::prelude::*;
use bevy_turborand::prelude::*;

mod game_assets;
use either::*;
use game_assets::*;
use leafwing_input_manager::prelude::*;

use crate::{components::ship::{Ship, Durability}, structs::value_in_range::ValueInRange};
use crate::components::player_ship::*;
use crate::components::*;
mod components;
use components::projectile::{Projectile, ProjectileBundle, projectile_system, RemainingRange};

mod doodles;
mod structs;
mod mouse_management;

#[derive(Clone, Copy, Eq, PartialEq, Debug, Hash, States, Default)]
enum GameState {
    #[default]
    Loading,
    GameStart,
    Playing,
    GameOver,
}

#[derive(Resource)]
struct MapInfo {
    asteroid_count: u32,
    radius: f32
}
impl Default for MapInfo {
    fn default() -> Self {
        Self {
            asteroid_count: 128 * 10 * 4,
            radius: 200. * 10.,
        }
    }
}


fn main() {
    // // DEBUG
    // std::env::set_var("RUST_BACKTRACE", "full");

    App::new()
        .insert_resource(ClearColor(Color::BLACK))
        .insert_resource(AmbientLight {
                color: Color::WHITE,
                brightness: 0.1
            })
        .insert_resource(MapInfo::default())

        .add_plugins(DefaultPlugins.set(WindowPlugin{
            primary_window: Some(Window{
                title: "SkyFighter".into(),
                // cursor: todo!(),
                // resolution: todo!(),
                // resize_constraints: todo!(),
                // resizable: todo!(),
                ..default()
            }),
            ..default()
        }))

        // DEGUB:
        .add_plugin(EditorPlugin)
        .add_plugin(FrameTimeDiagnosticsPlugin)

        .add_plugin(RngPlugin::default())

        .insert_resource(RapierConfiguration {
            gravity: Vec3::ZERO,
            ..default()
        })
        .add_plugin(RapierPhysicsPlugin::<NoUserData>::default())
        .add_plugin(RapierDebugRenderPlugin::default())

        .add_startup_system(GenAssets::init.in_base_set(StartupSet::PreStartup))

        .add_state::<GameState>()
        .add_loading_state(
            LoadingState::new(GameState::Loading)
                .continue_to_state(GameState::GameStart)
        )
        .add_collection_to_loading_state::<_, ShipModels>(GameState::Loading)
        .add_collection_to_loading_state::<_, BasicAssets>(GameState::Loading)

        .add_plugin(InputManagerPlugin::<CameraMovement>::default())

        .add_plugin(mouse_management::MouseManagement)

        .add_startup_system(setup_scene)
        .add_startup_system(setup_player)

        .add_system(player_steering)
        .add_system(player_rotation)
        .add_system(spawn_asteroid_in_front_of_player)

        .add_system(cannon_cooldown)
        .add_system(cannon_firing)
        .add_system(projectile_system)

        .add_plugin(doodles::DebugAndDoodlesPlugin)

        .run();
}


fn setup_scene(
    mut commands: Commands,
    map_info: Res<MapInfo>,
    gen_assets: Res<GenAssets>,
    mut rng: ResMut<GlobalRng>
) {
    // Spawn "Sun":

    commands.spawn(DirectionalLightBundle {
        directional_light: DirectionalLight {
            shadows_enabled: true,
            ..default()
        },
        transform: Transform {
            rotation: Quat::from_rotation_x(-std::f32::consts::FRAC_PI_4),
            ..default()
        },
        ..default()
    });

    // Spawn asteroids:

    let cube = gen_assets.cube_mesh.clone();
    let cube_material = gen_assets.white_material.clone();

    for _ in 0..map_info.asteroid_count {

        let pos_x = lerp(-map_info.radius, map_info.radius, rng.f32());
        let pos_y = lerp(-map_info.radius, map_info.radius, rng.f32());
        let pos_z = lerp(-map_info.radius, map_info.radius, rng.f32());

        // To make asteroids not spawn in proximity of (0, 0).
        if pos_x.abs() < 50.0 && pos_y.abs() < 50.0 {
            continue;
        }

        let position = Vec3::new(pos_x, pos_y, pos_z);
        let rotation = Quat::from_euler(EulerRot::XYZ, rng.f32(), rng.f32(), rng.f32());

        spawn_asteroid(&mut commands, cube.clone(), cube_material.clone(), position, rotation);
    }
}


fn spawn_asteroid(
    commands: &mut Commands,
    mesh: Handle<Mesh>,
    material: Handle<StandardMaterial>,
    position: Vec3,
    rotation: Quat,
) {

    // TODO: Make scale a param.
    let scale = Vec3::splat(5. * 5. * 5.);

    commands.spawn(PbrBundle {
        mesh: mesh,
        transform: Transform::from_translation(position)
            .with_rotation(rotation)
            .with_scale(scale),
        material: material,
        ..default()
    })
        .insert(Collider::cuboid(0.5, 0.5, 0.5))
        .insert(RigidBody::Dynamic)
        .insert(Dominance { groups: -1 });
}

/// Startup system
fn setup_player(
    mut commands: Commands,
    // ship_assets: Res<ShipModels>
    assets: Res<AssetServer>
    // basic_assets: Res<BasicAssets>,
) {
    const SHIP_POSTION: Vec3 = Vec3::new(0.0, 0.0, 0.0);
    let camera = commands.spawn(Camera3dBundle {
            transform: Transform::from_xyz(0.0, 1.0, 5.0)
                .looking_at(SHIP_POSTION + Vec3::new(0.0,1.0,0.0), Vec3::Y),
            // tonemapping: Tonemapping::
            // camera: Camera {
            //     hdr: true,
            //     ..default()
            // },
            ..default()
        })
        .insert(UiCameraConfig {
            show_ui: true,
            ..default()
        })
        .id();
    // TODO: Make it use AssetCollections
    let ship_model_asset: Handle<Scene> = assets.load("models/craft_speederA.glb#Scene0");
    let ship_model = commands.spawn(SceneBundle {
            // scene: ship_assets.craft_speederA.clone(),
            scene: ship_model_asset,
            transform: Transform::from_xyz(-2.0, 0.0, -1.5),
            ..default()
        })
        .id();
    let ship = commands.spawn(
            SpatialBundle::default()
        )
        .insert (PlayerShipBundle {
            player_ship: PlayerShip,
            ship: Ship::default(),
            durability: Durability(ValueInRange::<f32>::new(100.)),
            input: InputManagerBundle::<CameraMovement> {
                input_map: InputMap::default()
                    .insert(DualAxis::mouse_motion(), CameraMovement::Pan)
                    .build(),
                ..default()
            }
        })

        .insert(RigidBody::Dynamic)
        .insert(Collider::ball(2.))
        .insert(Sleeping::disabled())
        .insert(Ccd::enabled())
        .insert(Damping { linear_damping: 0.5, angular_damping: 1.0 })
        .insert(ExternalForce::default())
        .insert(ExternalImpulse::default())

        .push_children(&[
            ship_model,
            camera
        ])
        .id();
    // TODO: Make cannons as separate entities (children of ship)!
    commands.entity(ship).insert(Cannon {
            ignore_rigidbody: Some(ship),
            ..default()
        })
        ;
}



fn lerp<T>(a: T, b: T, t: f32) -> T
    where T: std::ops::Add<Output = T> + std::ops::Sub<Output = T> + std::ops::Mul<f32, Output = T> + std::marker::Copy {
    a + (b - a) * t
}

#[derive(Component)]
struct Cannon {
    damage: f32,
    muzzle_velocity: f32,
    cooldown: Timer,
    // TODO LOW: Maybe there are simpler more dynamic way of solving that.
    ignore_rigidbody: Option<Entity>,
    range: f32,
}

impl Default for Cannon {
    fn default() -> Self {
        Self {
            damage: 5.,
            muzzle_velocity: 10_000.,
            cooldown: Timer::from_seconds(0.4 / 1000., TimerMode::Repeating),
            ignore_rigidbody: None,
            range: 1000.
        }
    }
}

/// System
fn cannon_cooldown(
    mut query: Query<&mut Cannon>,
    time: Res<Time>,
) {
    query
        .par_iter_mut()
        .for_each_mut(|mut cannon| {
            cannon.cooldown.tick(time.delta());
        });
}

/// System
/// 
/// tags: mouse
fn cannon_firing(
    mut commands: Commands,
    cannons: Query<(&Cannon, &Transform)>,
    gen_assets: Res<GenAssets>,
    mouse_buttons: Res<Input<MouseButton>>,
) {
    // TODO: Make bullets inherit parent speed (I think that it would be quickest to use LastFrame<GlobalTransform>).
    let mesh = gen_assets.cube_mesh.clone();
    let material = gen_assets.white_material_emissive_5.clone();
    if mouse_buttons.pressed(MouseButton::Left) {
        // parents_with_velocity.get
        cannons.for_each(|(cannon, transform)| {
            // TODO: Create Timer with rollback and replace Cannon.cooldown with it.
            for i in 0..cannon.cooldown.times_finished_this_tick() {
                let velocity = transform.forward() * cannon.muzzle_velocity;
                fire_projectile(&mut commands, &mesh, &material, cannon.damage, transform.translation, velocity, cannon.ignore_rigidbody, cannon.range)
            }
        })
    }
}

/// `position` - Start position of projectile.
/// `ignore_rigidbody` - Entity of rigidbody to ignore (use firing ship most of the time).
fn fire_projectile(
    commands: &mut Commands,
    mesh: &Handle<Mesh>,
    material: &Handle<StandardMaterial>,
    damage: f32,
    position: Vec3,
    velocity: Vec3,
    ignore_rigidbody: Option<Entity>,
    range: f32,
) {
    commands.spawn(ProjectileBundle::Kinetic {
            pbr_bundle: PbrBundle {
                mesh: mesh.clone(),
                material: material.clone(),
                transform: Transform::from_translation(position)
                    .looking_at(velocity, Vec3::Y),
                ..default()
            },
            projectile: Projectile {
                velocity: velocity,
                kinetic_damage: damage,
                ignore_rigidbody: ignore_rigidbody,
            },
            range: RemainingRange::new(range),
        });
}