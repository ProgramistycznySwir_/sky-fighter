use std::ops::RangeInclusive;


#[derive(Clone)]
pub struct ValueInRange<T: Clone + PartialOrd> {
    pub value: T,
    pub range: RangeInclusive<T>
}

impl<T: Clone + PartialOrd> ValueInRange<T> {
    pub fn from_range(range: RangeInclusive<T>) -> ValueInRange<T> {
        Self {
            value: range.end().clone(),
            range: range
        }
    }
    pub fn with_value(self: &Self, val: T) -> ValueInRange<T> {
        Self {
            value: val,
            range: self.range.clone()
        }
    }

    pub fn is_min(self: &Self) -> bool {
        &(self.value) <= self.range.start()
    }
    pub fn is_max(self: &Self) -> bool {
        &(self.value) >= self.range.start()
    }
    /// Ensures that setted value is in range.
    pub fn set(self: &mut Self, val: T) -> &mut Self {
        self.value = val;
        self
    }
}

impl<T: Clone + PartialOrd + Default> ValueInRange<T> {
    pub fn new(max: T) -> ValueInRange<T> { Self::from_range(T::default()..=max) }
    pub fn zero(self: &Self) -> ValueInRange<T> {
        Self {
            value: T::default(),
            range: self.range.clone()
        }
    }
}

impl<T: Clone + PartialOrd> From<RangeInclusive<T>> for ValueInRange<T> {
    fn from(value: RangeInclusive<T>) -> Self {
        Self::from_range(value)
    }
}