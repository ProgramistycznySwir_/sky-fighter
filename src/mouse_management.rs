
use bevy::{prelude::*, window::{PrimaryWindow, CursorGrabMode}};

use crate::GameState;



pub struct MouseManagement;

impl Plugin for MouseManagement {
    fn build(&self, app: &mut App) {
        // add things to your app here
        app
            .add_system(turn_off_mouse_visibility.in_schedule(OnExit(GameState::Loading)))
            ;
    }
}


fn turn_off_mouse_visibility(
    window: Query<&mut Window, With<PrimaryWindow>>
) {
    set_cursor_visibility(window, false);
}

fn set_cursor_visibility(mut window: Query<&mut Window, With<PrimaryWindow>>, visibility: bool) {
    let Ok(mut window) = window.get_single_mut() else {return};
    window.cursor.visible = visibility;
    window.cursor.grab_mode = CursorGrabMode::Locked
}