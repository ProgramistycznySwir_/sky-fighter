use bevy::prelude::*;
use bevy_asset_loader::prelude::*;

#[derive(Resource, AssetCollection)]
pub struct ShipModels {
    
    #[asset(path = "models/craft_speederA.glb#Scene0")]
    pub craft_speederA: Handle<Scene>,
}

#[derive(Resource, AssetCollection)]
pub struct BasicAssets {
    // #[asset(path = "fonts/FiraSans-Bold.ttf")]
    // pub font_firaSans: Handle<Font>
}

/// Assets that are generated for game.
/// 
/// Don't forget to initialize it by calling `Self::init()`
#[derive(Resource)]
pub struct GenAssets {
    pub cube_mesh: Handle<Mesh>,
    pub white_material: Handle<StandardMaterial>,
    pub white_material_emissive_1: Handle<StandardMaterial>,
    pub white_material_emissive_3: Handle<StandardMaterial>,
    pub white_material_emissive_5: Handle<StandardMaterial>,
}

impl GenAssets {
    /// Startup system
    pub fn init(
        mut commands: Commands,
        _server: Res<AssetServer>,
        mut meshes: ResMut<Assets<Mesh>>,
        mut materials: ResMut<Assets<StandardMaterial>>,
    ) {
        commands.insert_resource(Self {
            cube_mesh: meshes.add(Mesh::from(shape::Cube { size: 1. })),
            white_material: materials.add(Color::WHITE.into()),
            white_material_emissive_1: gen_emissive_material(&mut materials, &1.),
            white_material_emissive_3: gen_emissive_material(&mut materials, &3.),
            white_material_emissive_5: gen_emissive_material(&mut materials, &5.),
        });
    }
}

fn gen_emissive_material(
    materials: &mut Assets<StandardMaterial>,
    intensity: &f32,
) -> Handle<StandardMaterial> {
    materials.add(StandardMaterial {
        // base_color: Color::WHITE.into(),
        emissive: Color::rgb_linear(*intensity, *intensity, *intensity).into(),
        unlit: true,
        ..default()
    })
}