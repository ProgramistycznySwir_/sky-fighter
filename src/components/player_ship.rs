use std::ops::Neg;

use bevy::{prelude::*, input::mouse::MouseMotion, math::Vec2Swizzles};
use bevy_rapier3d::prelude::*;
use leafwing_input_manager::{*, prelude::ActionState, orientation::Orientation};

use super::ship::*;
use crate::{spawn_asteroid, game_assets::GenAssets};


#[derive(Component)]
pub struct TeamHandle(u8);

#[derive(Component)]
pub struct PlayerShip;

// TODO: Finish and use.
#[derive(Bundle)]
pub struct PlayerShipBundle {
    pub player_ship: PlayerShip,
    pub ship: Ship,
    pub durability: Durability,
    pub input: InputManagerBundle<CameraMovement>,
}




/// System
/// 
/// tags: keyboard
pub fn player_steering(
    mut player_ships: Query<(&mut ExternalForce, &GlobalTransform, &Ship), With<PlayerShip>>,
    // TODO: Make it use some propper input
    keyboard: Res<Input<KeyCode>>,
) {
    if let Ok((mut ship_forces, transform, ship_stats)) = player_ships.get_single_mut() {
        let input = Vec3::ZERO
            + if !keyboard.pressed(KeyCode::W)      { Vec3::ZERO } else { Vec3::NEG_Z }
            + if !keyboard.pressed(KeyCode::S)      { Vec3::ZERO } else { Vec3::Z }
            + if !keyboard.pressed(KeyCode::A)      { Vec3::ZERO } else { Vec3::NEG_X }
            + if !keyboard.pressed(KeyCode::D)      { Vec3::ZERO } else { Vec3::X }
            + if !keyboard.pressed(KeyCode::Space)  { Vec3::ZERO } else { Vec3::Y }
            + if !keyboard.pressed(KeyCode::LShift) { Vec3::ZERO } else { Vec3::NEG_Y }
            ;
        let front: Quat = transform.to_scale_rotation_translation().1;
        ship_forces.force = ship_stats.get_force(&front, &input);
    }
}

// TODO: Move to separate file (something like input or something).
#[derive(Actionlike, Clone, Debug, Copy, PartialEq, Eq)]
pub enum CameraMovement {
    Pan,
}

/// System
///
/// tags: mouse, keyboard
pub fn player_rotation(
    mut player_ships: Query<(&mut ExternalImpulse, &GlobalTransform, &Ship, &ActionState<CameraMovement>), With<PlayerShip>>,
    // TODO: Make it use some propper input
    keyboard: Res<Input<KeyCode>>,
) {
    if let Ok((mut ship_impulses, transform, ship_stats, rotation_input)) = player_ships.get_single_mut() {
        let input: Vec3 = Vec3::ZERO
            + if !keyboard.pressed(KeyCode::Q) { Vec3::ZERO } else { Vec3::Z }
            + if !keyboard.pressed(KeyCode::E) { Vec3::ZERO } else { Vec3::NEG_Z }
            + rotation_input
                .axis_pair(CameraMovement::Pan)
                .unwrap_or_default()
                .xy()
                .clamp(Vec2::splat(-1.), Vec2::splat(1.))
                .yxxy()
                .truncate()
                .truncate()
                .neg()
                .extend(0.)
            ;
        let rotation = transform.to_scale_rotation_translation().1;

        ship_impulses.torque_impulse = ship_stats.get_impulse_torque(&rotation, &input);
    }
}

/// System, doodle
pub fn spawn_asteroid_in_front_of_player(
    mut commands: Commands,
    query: Query<&GlobalTransform, With<PlayerShip>>,
    gen_assets: Res<GenAssets>,
    keyboard: Res<Input<KeyCode>>,
) {
    if keyboard.just_pressed(KeyCode::P) {
        let mesh = gen_assets.cube_mesh.clone();
        let material = gen_assets.white_material.clone();

        let transform = query.single();

        const UNITS_IN_FRONT: f32 = 200.;
        let position = transform.translation() + query.single().forward() * UNITS_IN_FRONT;

        let rotation = transform.to_scale_rotation_translation().1;

        spawn_asteroid(&mut commands, mesh, material, position, rotation);
    }
}