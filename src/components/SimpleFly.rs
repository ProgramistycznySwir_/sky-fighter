#[derive(Component)]
struct SimpleFly{
    yaw:f32,
    pitch:f32,
    current_horizontal:f32,
    current_vertical:f32,
    follow:f32
}
impl Default for SimpleFly {
    fn default() -> Self {
        Self {
            yaw: 0.0,
            pitch: 0.0,
            current_horizontal: 0.0,
            current_vertical: 0.0,
            follow: 1.6
        }
    }
}