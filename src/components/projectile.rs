use bevy::{prelude::*, ecs::system::Command};
use bevy_rapier3d::prelude::*;

use super::ship::Durability;

/// Projectile dealing kinetic damage.
/// 
/// add Explosive and ProximityFuse for more varied projectiles.
#[derive(Component)]
pub struct Projectile {
    pub velocity: Vec3,
    pub kinetic_damage: f32,
    pub ignore_rigidbody: Option<Entity>,
    // pub collision_groups: CollisionGroups,
}

/// Used when we want to destroy some entity that is supposed to die after traveling some distance.
#[derive(Component)]
pub struct RemainingRange(f32);
impl RemainingRange {
    pub fn new(range: f32) -> Self {
        RemainingRange(range)
    }
    pub fn is_zero(self: &Self) -> bool {
        self.0 <= 0.
    }
    pub fn dec(self: &mut Self, distance: f32) -> &mut Self {
        self.0 -= distance;
        self
    }
}

#[derive(Component)]
pub struct Explosive {
    damage: ExplosiveDamage
}

#[derive(Component)]
pub struct ProximityFuse {
    stats: FuseStats
}

// pub enum ProjectileType {
//     Kinetic,
//     Explosive{damage: ExplosiveDamage},
//     // ProximityFuse{damage: ExplosiveDamage, fuse: FuseStats},
// }

pub struct ExplosiveDamage {
    range: f32,
    damage: f32,
    damage_falloff: DamageFalloff
}

pub enum DamageFalloff {
    None,
    /// Divided in two zones: DamageFalloff::None up to range of `after` and then DamageFalloff::Linear.
    SemiLinear{after: f32},
    Linear,
    Exponential{power: i32},
}

pub struct FuseStats {
    pub range: f32,
    pub mode: FuseMode
}

pub enum FuseMode {
    Explode_OnEnterRange,
    /// Explodes when distance from target starts increasing while in fuse range (usefull for exploding closest to target).
    Explode_OnStartLeaving,
    /// Explodes when target leaves fuse range (usefull for attacking back of target).
    Explode_OnLeaveRange
}

pub struct ProjectileWeapon {
    pub muzzle_velocity: f32,
    /// unit: degrees
    pub spread: f32,
    pub damage: f32,

    // pub ammo: ProjectileType,
    
}

/// System.
pub fn projectile_system(
    mut commands: Commands,
    mut projectiles: Query<(Entity, &mut Transform, &Projectile, Option<&mut RemainingRange>)>,
    mut query: Query<&mut Durability>,
    physics: Res<RapierContext>,
    time: Res<Time>,

) {
    projectiles.iter_mut().for_each(|(entity, mut transform, projectile, remaining_range)| {
        let distance = projectile.velocity.length() * time.delta_seconds();
        if let Some(mut remaining_range) = remaining_range {
            if remaining_range.dec(distance).is_zero() {
                commands.entity(entity).despawn_recursive();
            }
        }
        let result = physics.cast_ray(
            transform.translation,
            projectile.velocity.normalize(),
            distance,
            true,
            match projectile.ignore_rigidbody {
                Some(rb) => QueryFilter::new().exclude_rigid_body(rb),
                None => QueryFilter::default()
            }
        );
        if let Some((hitted, hit_distance)) = result {
            if let Ok(mut hitted_durability) = query.get_mut(hitted) {
                hitted_durability.do_damage(&projectile.kinetic_damage);
            };
            commands.entity(entity).despawn_recursive();
        }
        else {
            transform.translation += projectile.velocity * time.delta_seconds();
        }
    });
}

pub mod ProjectileBundle {
    use bevy::prelude::*;

    use super::{Projectile, RemainingRange};

    #[derive(Bundle)]
    pub struct Kinetic {
        pub pbr_bundle: PbrBundle,
        pub projectile: Projectile,
        pub range: RemainingRange,
    }
}