use core::f32;
// use std::ops::RangeInclusive;

use bevy::prelude::*;
use leafwing_input_manager::orientation::Orientation;

use crate::structs::value_in_range::ValueInRange;


#[derive(Component)]
pub struct Ship {
    engine_power: f32,
    /// # unit: radians per second.
    rotation_speed: f32,
}

impl Default for Ship {
    fn default() -> Self {
        Self {
            engine_power: 25. * 1000. * 2.,
            rotation_speed: std::f32::consts::TAU * 0.5 / 2.
        }
    }
}

impl Ship {
    /// Returns force applied to RigidBody from given `throttle`
    ///
    /// `front` - __Normalized__ vector pointing to front of ship. </br>
    /// `throttle` - Value in range `-1.0..1.0`.
    pub fn get_force(&self, ship_rotation: &Quat, throttle: &Vec3) -> Vec3 {
        Self::get_force_from_power(self.engine_power, ship_rotation, throttle)
    }
    /// Use `Self::get_force(...)` instead.
    pub fn get_force_from_power(power: f32, ship_rotation: &Quat, throttle: &Vec3) -> Vec3 {
        // TODO: For propper use of `front` please use Vec2::X instead of Vec2::Y
        ship_rotation.mul_vec3(power * throttle.clamp(Vec3::splat(-1.), Vec3::splat(1.)))
    }

    /// `steering` - Values in range `-1.0..1.0`.
    pub fn get_impulse_torque(&self, ship_rotation: &Quat, steering_axis: &Vec3) -> Vec3 {
        ship_rotation.mul_vec3(self.rotation_speed * steering_axis.clamp(Vec3::splat(-1.), Vec3::splat(1.)))
    }
}


#[derive(Component)]
pub struct Durability(pub ValueInRange<f32>);

impl Durability {
    pub fn do_damage(self: &mut Self, dmg: &f32) -> &mut Self {
        self.0.value -= dmg;
        self
    }
}





